# Machine learning_RNAseq

(Los datos brutos han sido eliminados ya que eran demasiado pesados)

En este repositorio voy a recrear el articulo [Predicting age from the transcriptome of human dermal fibroblasts](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE113957).
En este artículo pretenden predecir la edad de los individuos utilizando los datos transcriptómicos de células de la piel (fibroblastos). 
Estos datos consisten en un perfil de expresión genetica. Por lo que lo que vamos a tener es el conteo de número de transcritos de cada gen por cada individuo.

# Obtención de los datos

Para la obtención de la matriz de datos (genes X indiviuos) he tenido que descargar los datos en formato SRA desde NCBI y convertirlos en FASTQ. 
Para ello he utilizado el script [fromSRAtoFASTQ.sh](Dataset2/analisis/fromSRAtoFASTQ.sh).
Una vez obtenidos los datos en FASTQ hay que realizar el análisis bioinformático.

# Análisis Bioinformático

1º-> Selección de lecturas de calidad con fastp  
2º-> Generación de informes de calidad con FASTQC    
3º-> Alineamiento de las secuencias con el genoma grch38 con el software HISAT2  
4º-> Conteo de las secuencias alineadas con los genes  

Este proceso lo llevo a cabo con el script [RNASeq_driver_script.sh](Dataset2/analisis/RNASeq_driver_script.sh).

# Primer análisis

Como he podido observar parece que en los datos hay 2 poblaciones diferenciadas, una población joven y otra mas envejecida. Esto se puede observar en el histograma de este [documento](https://gitlab.com/guillermosanz92/machine-learning_rnaseq/-/blob/master/Dataset2/analisis/Analisis.nb.html).
así que voy a empezar estudiando los genes diferencialmente expresados entre estas dos poblaciones. 

((Posteriormente seguiré con un modelo predictivo, pero no se si haré un modelo de clasificación entre joven y viejo o un modelo de regresión para predecir la edad))


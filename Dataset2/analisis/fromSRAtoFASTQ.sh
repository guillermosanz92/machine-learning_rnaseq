#!/bin/bash

rawReadsDir="$PWD/1_raw_reads"

for i in $rawReadsDir/SRR*
	
	do
	basei=$(basename $i)
	echo "Transformando el archivo $basei en .fastq"
	date
	fasterq-dump $i -O $rawReadsDir -e 4
	echo "Transformacion del archivo $basei completada"
	date
done

---
title: "Dataset4"
output: html_notebook
---

Librerias:

```{r}
library(edgeR)
library(AnnotationDbi)
library(org.Hs.eg.db)
library(tidyverse)
library(plotly)
library(EnsDb.Hsapiens.v86)
library(VennDiagram)
library(ComplexHeatmap)
```


# FeatureCounts 

# Importacion de los datos:

```{r datos, message=FALSE, warning=FALSE}

counts <- read.delim("/media/HDD_5TB/Documentos/Proyectos/10.Datasets/Aging/Fibroblastos/Dataset2/analisis/4_counts/featureCounts.tsv", comment.char="#")

pattern <- c(".+SRR")
names(counts) <- gsub(pattern,"SRR", names(counts))
names(counts) <- gsub(".bam","", names(counts))
names(counts) <- gsub("\\.1","", names(counts))

length_counts <- counts$Length
row.names(counts) <- counts$GeneID


Metadata <- read.delim("/media/HDD_5TB/Documentos/Proyectos/10.Datasets/Aging/Fibroblastos/Dataset2/analisis/Metadata")
Metadata$Edad.del.sujeto <- c(12, 25, 25, 29, 29, 30, 30, 44, 41, 46, 50, 47, 44, 45, 61, 64, 66, 67, 67, 68, 68, 69, 69, 83, 83, 84, 84, 87, 87, 88, 90, 91, 92, 96, 1, 2, 3, 3, 5, 6, 7, 7, 8, 8, 9, 10, 11, 12, 19, 20, 31, 32, 33, 37, 37, 78, 80, 94, 89, 87, 89, 86, 92, 87, 22, 29, 23, 26, 21, 67, 67, 68, 69, 8, 2.25, 5, 1, 24, 26, 26, 28, 30, 30, 43, 47, 50, 42, 41, 43, 46, 46, 47, 62, 62, 63, 83, 84, 84, 84, 84, 84, 86, 86, 86, 87, 87, 89, 10, 11, 13, 16, 17, 17, 19, 19, 19, 20, 31, 32, 33, 37, 39, 51, 52, 55, 57, 60, 71, 75, 90, 25, 24, 68, 82, 66, 70, 8, 3.75, 40.66, 8.5, 6.92, 8.84, 3)

Metadata <- mutate(Metadata, Edad.relativa = ifelse(Edad.del.sujeto >= 60 & Tratamiento == c("Normal"),c("Old"), ifelse(Edad.del.sujeto <= 30 & Tratamiento == "Normal" , c("Young"), "No_estudiado")))
########________Parametros________########
pvalue_val <- 0.05
logFC_val <- 0.5
##########################################
muestras_int <- subset(Metadata, Edad.relativa == "Young" | Edad.relativa == "Old")

hist(muestras_int$Edad.del.sujeto,freq = T, breaks = 200)


```

## ALL

Filtrado:

```{r message=FALSE, warning=FALSE}
y <- DGEList(counts[7:149], genes = counts[1], group = Metadata$Edad.relativa)

keep <- filterByExpr(y, design = NULL, # No se va a especificar ni el diseño ni el grupo ya que el filtrado se va a realizar sobre todos los datos
                     group = NULL, 
                     lib.size = NULL, # Por defecto es colSums(y)
                     min.count = 10, # Se requiere un recuento mínimo para al menos algunas muestras.
                     min.total.count = 15, # Se requiere un recuento total mínimo.
                     large.n = 3, # Numero de muestras del grupo mayor
                     min.prop = 0.7) # Propocon <- makeContrasts("Metadata$CondicionCBD"-"Metadata$CondicionControl"rción mínima de muestras del grupo más pequeño que expresan el gen.

# Filtrado de conteo de genes. Se mantendrán los genes que logran al menos 10 (min.count) cpm (conteos por millón) de tres (large.n (3 son el numero maximo de muestras por condicion. En este caso tenemos 3 muestras en cada condicion, por lo que no hay duda)), Ademas, se requiere que cada gen que se quede tenga al menos 15 lecturas (min.total.count) en todas las muestras.

y <- y[keep, , keep.lib.sizes=FALSE]


## Normalizado para EdgeR:


y <- calcNormFactors(y, method = "TMM")

design <- model.matrix( ~ 0 + Metadata$Edad.relativa)

colnames(design) <- levels(factor(Metadata$Edad.relativa))

y <- estimateDisp(y, design, robust=TRUE)

Norm_Counts_edgeR <- cpm(y, log = TRUE, prior.count = 3)



## ALL Young VS Old


con <- makeContrasts(Old - Young, levels=design)
fit <- glmQLFit(y, contrast = con, robust=TRUE) # Se ajusta el modelo glm BN QL (Generalized Linear Model, Negative Binomial, Quasi-Likelihood)
qlf <- glmQLFTest(fit, contrast=con)

DE_edgeR <- topTags(qlf, n = Inf)

TopT <- data.frame(DE_edgeR$table)
TopT <- TopT %>% mutate(threshold = ifelse(logFC >= logFC_val & PValue<pvalue_val,"Upregulated", ifelse(logFC<=-logFC_val& PValue<pvalue_val , "Downregulated", "No sig")))

Symbolos <- ensembldb::select(EnsDb.Hsapiens.v86, keys = as.character(TopT$Geneid), columns =  "SYMBOL", keytype = "GENEID")
rownames(TopT) <- TopT$Geneid


for (i in rownames(TopT)) {
  if (i %in% Symbolos$GENEID) {
    TopT[i, "Symbol"] <- Symbolos$SYMBOL[Symbolos$GENEID == i]
  }
  else{
    TopT[i, "Symbol"] <- "NA"
  }
}

Dataset2_Old_vs_Young <- TopT
write.table(TopT, file = "DEGs/Dataset2_Old_vs_Young", row.names = FALSE)

resumen <- data.frame(Downregulated = dim(Dataset2_Old_vs_Young[Dataset2_Old_vs_Young$threshold=="Downregulated",])[1], Upregulated = dim(Dataset2_Old_vs_Young[Dataset2_Old_vs_Young$threshold=="Upregulated",])[1])

resumen
```

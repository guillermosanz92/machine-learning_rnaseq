#!/bin/bash

# directories
rawReadsDir="$PWD/1_raw_reads"
procReadsDir="$PWD/2_processed_reads"
alignmentsDir="$PWD/3_alignments"
countsDir="$PWD/4_counts"
qcDir="$PWD/5_QC"

# INDEX AND ANNOTATION FILES 
hisatIndex="/media/HDD_5TB/Documentos/Proyectos/0.Utiles/grch38/genome"
gtfFile="/media/HDD_5TB/Documentos/Proyectos/0.Utiles/grch38/Homo_sapiens.GRCh38.99.gtf"


#########
# START #
#########

echo "STARTING DIVER SCRIPT ANALYSIS.."

date

# CHECK IF RAW READS FOLDER EXISTS
if [ ! -d $rawReadsDir ]; then
	echo "Raw reads folder does not exists"
	exit
fi

#########
# fastp #
#########
if [ ! -d $procReadsDir ]; then

	# create directory for trimmed reads
	mkdir -p $procReadsDir
	mkdir -p $procReadsDir/JSON
	mkdir -p $procReadsDir/HTML

	# looping through fastq files
	for i in $rawReadsDir/*.fastq

		do
		# filebasename
		samp=$(basename $i)
		echo "Starting trimming for $samp..."

		fastp -i $i \
			-o $procReadsDir/$samp \
			-j $procReadsDir/JSON/${samp/_fastp.json} \
			-h $procReadsDir/HTML/${samp/_fastp.html} \
			--thread 4 \
			--cut_right \
			--cut_right_window_size 4 \
			--cut_right_mean_quality 15 \
			--length_required 20 

		echo "Fastp for $samp completed"
/biokit/
	done

	echo "Fastp completed"

else

	echo "The processed reads folder already exists, skipping pre-processing with fastp !!"

fi

##########
# FASTQC #
##########
if [ ! -d $qcDir ]; then

	mkdir -p $qcDir

	fastqc -t 6 $procReadsDir/*.fastq -o $qcDir

else

	echo "The processed reads QC folder already exists, skipping processed reads QC !!"

fi

##########
# HISAT2 #
##########
if [ ! -d $alignmentsDir ]; then

	# create directory for alignments and summary
	mkdir -p $alignmentsDir/bam
	mkdir -p $alignmentsDir/summary

	# looping through fastq files
	for i in $procReadsDir/*.fastq
		do

		# filebasename
		samp=$(basename $i)
		echo "Aligning... $samp"

		hisat2 -p 3 -x $hisatIndex --new-summary --summary-file $alignmentsDir/summary/${samp/.fastq.gz/_summary.txt} \
			-U $i | samtools sort -@ 3 -O BAM -o $alignmentsDir/bam/${samp/.fastq/.bam}

	done

	echo "Alignment with HISAT2 completed!"

else

	echo "The hisat2 folder already exists, skipping alignment with hisat2 !!"

fi

#################
# featureCounts #
#################
if [ ! -d $countsDir ]; then

	mkdir -p $countsDir

	featureCounts -a $gtfFile -T 6 -o $countsDir/featureCounts.tsv $alignmentsDir/bam/*.bam

	sed -r 's#[^\t]+/([^\/\t]+)\.[bs]am#\1#g' $countsDir/featureCounts.tsv | sed '1d' | cut -f 1,7- > $countsDir/featureCountsMatrix.tsv

else

	echo "The featureCounts folder already exists, skipping counting with featureCounts !!"

fi


